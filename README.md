# 4DPlotter

![alt text](https://gitlab.com/FrVi/4DPlotter/raw/09cfb9ed1048d000c964bc1bec767e2e6b393bac/presentation/ressources/4Dplotter.png)
<br />

This program displays complex-valued functions of a complex variable z in 3D.

I uses a smart trick to add a fourth dimension using colors. 

This was my TIPE project in 2009-2010. 

The latest version was done on 26/07/2010.