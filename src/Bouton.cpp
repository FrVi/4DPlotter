#include "Bouton.h"

Bouton::Bouton()
{
	set(0,10,0,10,"vide",NULL,txt_r,txt_g,txt_b);
}

Bouton::Bouton(int Xmin,int Xmax,int Ymin, int Ymax,string Str,void (*Fonction)(Graph_coord &),GLubyte red,GLubyte green,GLubyte blue)
{
	set(Xmin,Xmax,Ymin,Ymax,Str,Fonction,red,green,blue);
}

void Bouton::set(int Xmin,int Xmax,int Ymin, int Ymax,string Str,void (*Fonction)(Graph_coord &),GLubyte r,GLubyte g,GLubyte b)
{
	xmin=Xmin;
	xmax=Xmax;
	ymin=Ymin;
	ymax=Ymax;
	str=Str;
	fonction=Fonction;
	red=r;
	green=g;
	blue=b;
}

Bouton::~Bouton(){}

bool Bouton::appartient(SDL_MouseButtonEvent button)
{
	return (button.y<=hauteur-8*ymin && button.y>=hauteur-8*ymax && button.x>=8*xmin && button.x<=8*xmax);
}


void Bouton::afficher()
{
	glColor3ub(red,green,blue);
	glBegin(GL_LINE_STRIP);

		glVertex2d(xmax,ymin);
		glVertex2d(xmax,ymax);
		glVertex2d(xmin,ymax);
		glVertex2d(xmin-0.1,ymin);
		glVertex2d(xmax,ymin);

	glEnd();

	glEnable(GL_TEXTURE_2D);
		glFontTextOut(str, xmin+(xmax-xmin-str.size())/2,(7*ymax+2*ymin)/9, 0);
	glDisable(GL_TEXTURE_2D);
}

//FONCTIONS DES BOUTONS
void Vue_x(Graph_coord &GL_coord)
{
	GL_coord.angleX =0.0;
	GL_coord.angleY =0.0;
	GL_coord.angleZ =-90.0;
	GL_coord.Zfact=50.0; 
}

void Vue_y(Graph_coord &GL_coord)
{
	GL_coord.angleX =0.0;
	GL_coord.angleY =0.0;
	GL_coord.angleZ =0.0;
	GL_coord.Zfact=50.0; 
}

void Vue_z(Graph_coord &GL_coord)
{
	GL_coord.angleX =90.0;
	GL_coord.angleY =0.0;
	GL_coord.angleZ =0.0;
	GL_coord.Zfact=50.0; 
}

void Vue_diag(Graph_coord &GL_coord)
{
	GL_coord.angleX =45.0;
	GL_coord.angleY =0.0;
	GL_coord.angleZ =225.0;
	GL_coord.Zfact=50.0; 
}

void incr_largeur(Graph_coord &GL_coord)
{
	GL_coord.xmin*=FACTOR;
	GL_coord.xmax*=FACTOR;
}

void decr_largeur(Graph_coord &GL_coord)
{
	GL_coord.xmin/=FACTOR;
	GL_coord.xmax/=FACTOR;
}

void incr_longueur(Graph_coord &GL_coord)
{
	GL_coord.ymin*=FACTOR;
	GL_coord.ymax*=FACTOR;
}

void decr_longueur(Graph_coord &GL_coord)
{
	GL_coord.ymin/=FACTOR;
	GL_coord.ymax/=FACTOR;
}

void incr_x_centre(Graph_coord &GL_coord)
{
	GL_coord.xmin+=INCR;
	GL_coord.xmax+=INCR;
}

void decr_x_centre(Graph_coord &GL_coord)
{
	GL_coord.xmin-=INCR;
	GL_coord.xmax-=INCR;
}

void incr_y_centre(Graph_coord &GL_coord)
{
	GL_coord.ymin+=INCR;
	GL_coord.ymax+=INCR;
}

void decr_y_centre(Graph_coord &GL_coord)
{
	GL_coord.ymin-=INCR;
	GL_coord.ymax-=INCR;
}