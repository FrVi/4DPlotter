//GLFONT.H -- Header for GLFONT.CPP
//Copyright (c) 1998 Brad Fish 

#ifndef GLFONTh
#define GLFONTh

	#include <iostream>
	#include "Arbre binaire.h"

typedef struct GLFONTCHAR GLFONTCHAR;
struct GLFONTCHAR
{
	float dx, dy;
	float tx1, ty1;
	float tx2, ty2;
};

typedef struct GLFONT GLFONT;
struct GLFONT
{
	int Tex;
	int TexWidth, TexHeight;
	int IntStart, IntEnd;
	GLFONTCHAR *Char;
};

int glFontCreate (GLFONT *Font, char *FileName, int Tex);
void glFontDestroy (GLFONT *Font);
void glFontBegin (GLFONT *Font);
void glFontEnd (void);
void glFontTextOut (string String, float x, float y,float z);

#endif