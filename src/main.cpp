#include "Plot.h"

int main(int argc, char *argv[]) 
{  
//initialisation de SDL
	SDL_Init(SDL_INIT_VIDEO);
	atexit(SDL_Quit);
	SDL_WM_SetCaption("SDL OPENGL 4D plotter (Frederic Vitzikam)", NULL);
	SDL_SetVideoMode(largeur,hauteur, 32, SDL_OPENGL);
	SDL_EnableUNICODE(1);

//initialisation de OpenGl
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective(70,(double)largeur/hauteur,1,2000);

//chargement de la police
	GLFONT font;
	if(	!glFontCreate (&font,"police", 0)){ return 0; }//erreur ouverture du fichier

//cr�ation d'une display list
	GLuint list;
	list = glGenLists(1);
	if(list == 0){	return 0; }

//cr�ation des boutons
Bouton Bouton[NBR_BOUTON];

// Bouton.set(       Xmin,        Xmax, Ymin,Ymax, texte,  fonction, rouge, vert, bleu )
Bouton[0].set(largeur/8-6,  largeur/8-1,  11, 15, "diag", &Vue_diag, txt_r, txt_g,txt_b);
Bouton[1].set(largeur/8-11, largeur/8-7,  11, 15, "y",    &Vue_y,        0,     0,  255);
Bouton[2].set(largeur/8-16, largeur/8-12, 11, 15, "z",    &Vue_z,       30,   155,   67);
Bouton[3].set(largeur/8-21, largeur/8-17, 11, 15, "x",    &Vue_x,      255,     0,    0);

Bouton[4].set(largeur/8-30, largeur/8-26, 11, 15, "+", &incr_largeur, txt_r, txt_g, txt_b);
Bouton[5].set(largeur/8-35, largeur/8-31, 11, 15, "-", &decr_largeur, txt_r, txt_g, txt_b);
Bouton[6].set(largeur/8-30, largeur/8-26, 1,   5, "+", &incr_longueur, txt_r, txt_g, txt_b);
Bouton[7].set(largeur/8-35, largeur/8-31, 1,   5, "-", &decr_longueur, txt_r, txt_g, txt_b);
Bouton[8].set(largeur/8-44, largeur/8-40, 11,  15,"+", &incr_x_centre, txt_r, txt_g, txt_b);
Bouton[9].set(largeur/8-49, largeur/8-45, 11,  15,"-", &decr_x_centre, txt_r, txt_g, txt_b);
Bouton[10].set(largeur/8-44, largeur/8-40, 1, 5,  "+", &incr_y_centre, txt_r, txt_g, txt_b);
Bouton[11].set(largeur/8-49, largeur/8-45, 1, 5,  "-", &decr_y_centre, txt_r, txt_g, txt_b);
Bouton[12].set(largeur/8-4, largeur/8-1,   1, 5,  "+", &incr_maillage, txt_r, txt_g, txt_b);
Bouton[13].set(largeur/8-8, largeur/8-5,   1, 5,  "-", &decr_maillage, txt_r, txt_g, txt_b);

Bouton[14].set(largeur/8-63, largeur/8-55, 11, 15,  "reset", &reset, txt_r, txt_g, txt_b);

//boutons non standards (pas de fonction proprement associ�e)
Bouton[15].set(largeur/8-63, largeur/8-55,  1,  5,  "reflet",  NULL, txt_r, txt_g, txt_b);
Bouton[16].set(largeur/8-21, largeur/8-12,  6, 10,  "axes",    NULL, txt_r, txt_g, txt_b);
Bouton[17].set(largeur/8-11, largeur/8-1,   6, 10,  "maillage",NULL, txt_r, txt_g, txt_b);

//Position de la cam�ra et largeur du graphe 
//                   angleZ,angleX,angleY,Zfact,xmin,xmax,ymin,ymax                  
Graph_coord GL_coord = {0.0, 0.0, 0.0, 20.0, XMIN, XMAX, YMIN, YMAX};

Uint32 ellapsed_time=0;
Uint32 start_time;

string Str;

Str="(z^2-1)*(z-2-i)/(z+2)/10";
Str=Str.substr(0,Str.find("\n"));//retrait du "\n" terminal

Noeud* arbre=NULL;

arbre=Construire_arbre(Str,arbre);

int mode=1;
int graphe=0;
int reflet=0;

SDLKey Clav[SDLK_LAST]={(SDLKey)0};

SDL_Event event;

string STR="";

SDL_EnableKeyRepeat(500, 20);

bool continuer=true;
double Zsize=12.0;

Plot plot(&mode,&reflet,Bouton,&list,&font,&GL_coord,arbre,&graphe,"",Str);
plot.Init(0);
plot.Dessiner(Zsize);

while(continuer)
{
	start_time = SDL_GetTicks();     
        
    while (SDL_PollEvent(&event))
    {
	  if ( event.type == SDL_KEYDOWN ) 
      {  
		if(event.key.keysym.sym==SDLK_ESCAPE){continuer=false; break;}

		arbre = plot.Traduction(Clav, &event, Zsize);//gestion de la touche
	  }  
	  else if(event.type==SDL_QUIT)
	  {
		continuer=false; 
		break;
	  }
	  else if(event.type==SDL_MOUSEBUTTONDOWN)
	  {
	    if(event.button.y<hauteur-h_menu)
		{
		   if(event.button.button == SDL_BUTTON_WHEELUP && Zsize<40)    Zsize*=1.2;
		  else if(event.button.button == SDL_BUTTON_WHEELDOWN && Zsize>0.1) Zsize*=0.83;
		  else if(event.button.button == SDL_BUTTON_LEFT) plot.camera(event,Zsize);
		  else if(event.button.button == SDL_BUTTON_RIGHT)plot.echelle(event,Zsize);
		}
		else{	plot.boutons(event,Zsize);	}
	  }

	}

	plot.Dessiner(Zsize);

    ellapsed_time=SDL_GetTicks()-start_time;

	if (ellapsed_time<10){	SDL_Delay(10-ellapsed_time);	}
}

glFontDestroy(&font);

delete arbre;//tout le reste est lib�r� via les destructeurs 

return EXIT_SUCCESS; 
} 