#include "Plot.h"

int dim=230;

Plot::Plot(int *m,int *r,Bouton *B,GLuint *l,GLFONT *F,Graph_coord *G,Noeud* a,int *g,string STR, string STR2)
{
	reflet=r;
	mode=m;
	bouton=B;
	list=l;
	Font=F;
	GL_coord=G;
	arbre=a;
	graphe=g;
	Str=STR;
	Str2=STR2;
	Erreur="";
	position=0;
}

Plot::~Plot()
{
	glDeleteLists(*list, 1);
}


void Plot::Init(int m)
{
	double xmin=GL_coord->xmin;
	double ymin=GL_coord->ymin;
	double xmax=GL_coord->xmax;
	double ymax=GL_coord->ymax;

	Coordonnees *tab=NULL;
	tab=new Coordonnees[dim];

	int i,j;
	double x,y;

	complex<double> z(0.0,0.0);
	complex<double> t(0.0,0.0);

	for(j=0;j<dim;j++)
	{
		x=xmin;
		y=(ymax-ymin)/(dim-1)*j+ymin;
		z.real(x);
		z.imag(y);
		t=arbre->calculer(z);  
		set_tab(tab,t,j,m);
	}

	Uint32 color=0;

	glNewList(*list, GL_COMPILE);//on initialise la display list

/* on dessine les triangles dans cet ordre:
. .
. .
5-6-...
|\|
3-4-...
|\|
1-2-...
on appelle chaque sommet seulement 2 fois
*/

	double imin =-(xmax+xmin)/2.0*(dim-1)/(xmax-xmin);
	double jmin =-(ymax+ymin)/2.0*(dim-1)/(ymax-ymin);

	GLenum MODE;

	if(*graphe==0){MODE=GL_TRIANGLE_STRIP;}
	else{MODE=GL_LINE_STRIP;}

	if(MODE==GL_LINE_STRIP)
	{
		glBegin(MODE);
		for(j=0;j<dim;j++)
		{
            color=tab[j].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255)); 
            glVertex3d(200.0/(dim-1)*(-imin)-100.0,200.0/(dim-1)*(j-jmin)-100.0,tab[j].z);
		}
		glEnd();
	}

    for(i=0;i<dim;i++)
    { 
		glBegin(MODE);

		for(j=0;j<dim-1;j=j+2)           
		{                   
			color=tab[j].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255));  
            glVertex3d(200.0/(dim-1)*(i-imin)-100.0,200.0/(dim-1)*(j-jmin)-100.0,tab[j].z); 
                
			x=(xmax-xmin)/(dim-1)*(i+1)+xmin;//on remplace le point (i;j) par le point (i+1;j)   permet d'avoir un tableau � une dimension pour les
			y=(ymax-ymin)/(dim-1)*j+ymin;    //sommets au lieu de 2 : on remplace les sommets de la colonne j par ceux de la colonne j+1 des qu'ils ne
			z.real(x);						  //servent plus
			z.imag(y);
			t=arbre->calculer(z);

			set_tab(tab,t,j,m);

            color=tab[j].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255)); 
            glVertex3d(200.0/(dim-1)*(i+1-imin)-100.0,200.0/(dim-1)*(j-jmin)-100.0,tab[j].z);
         		                   
			color=tab[j+1].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255)); 
            glVertex3d(200.0/(dim-1)*(i-imin)-100.0,200.0/(dim-1)*(j+1-jmin)-100.0,tab[j+1].z);

			x=(xmax-xmin)/(dim-1)*(i+1)+xmin;//on remplace le point (i;j+1) par le point (i+1;j+1) 
			y=(ymax-ymin)/(dim-1)*(j+1)+ymin;
			z.real(x);
			z.imag(y);
			t=arbre->calculer(z);

			set_tab(tab,t,j+1,m);

            color=tab[j+1].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255)); 
            glVertex3d(200.0/(dim-1)*(i+1-imin)-100.0,200.0/(dim-1)*(j+1-jmin)-100.0,tab[j+1].z);
		}
		glEnd();
    }

	if(MODE==GL_LINE_STRIP)
	{
		glBegin(MODE);
		for(j=0;j<dim;j++)
		{
            color=tab[j].color;
            glColor3ub((color>>16),((color>>8)&255),(color&255)); 
            glVertex3d(200.0/(dim-1)*(i-imin)-100.0,200.0/(dim-1)*(j-jmin)-100.0,tab[j].z);
		}
		glEnd();
	}


	if(*reflet==1 && m!=1)
	{
		Init(1);
	}

	glEndList(); //fin de l'initialisation de la display list
	delete[] tab;
}

void Plot::Dessiner(double Zsize)
{
	glClearColor(1,1,1,0);	
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    gluLookAt(0,-8*GL_coord->Zfact,0,0,0,0,0,0,1);
    glRotated(GL_coord->angleX,1,0,0);
	glRotated(GL_coord->angleY,0,1,0);
    glRotated(GL_coord->angleZ,0,0,1);

	glScaled(1,1,Zsize);
    
	glCallList(*list);//on utilise la display list
	Repere(Zsize);
	Menu();

    glFlush();
    SDL_GL_SwapBuffers();
}

void Plot::Menu()
{

glDisable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // on enregistre les param�tres pour la 3D
	glLoadIdentity();
	gluOrtho2D(0, largeur, 0, hauteur);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_QUADS);
		glColor3ub(menu_r,menu_g,menu_b);
		glVertex2d(0,0);
		glVertex2d(largeur,0);
		glVertex2d(largeur,h_menu);
		glVertex2d(0,h_menu);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

glFontBegin(Font);

	glScaled(8, 8, 8);
	glTranslatef(0, 0, 0);

	glColor3ub(txt_r,txt_g,txt_b);

	for(int i=0;i<NBR_BOUTON;i++)
	{	
		bouton[i].afficher();
	}

	glEnable(GL_TEXTURE_2D);//cette partie utilise les textures
	glFontTextOut("Equation de la surface actuelle : f(z)=", 1, 14, 0);
	glFontTextOut(Str2.substr(0,24), 36, 14, 0);

	if(Str2.length()>24)
	{
		glFontTextOut(Str2.substr(24,53), 1, 12, 0);
	}

	glFontTextOut("entree : ", 1, 6, 0);

	if(Erreur!="")
	{
		glColor3ub(255,0,0);
		glFontTextOut(Erreur, 1, 9, 0);
		glColor3ub(txt_r,txt_g,txt_b);
	}

	string Str3=Str;
	Str3.insert(position,"|");

	glFontTextOut(Str3.substr(0,33), 9, 6, 0);

	if(Str.length()>33)
	{
		glFontTextOut(Str3.substr(33,38), 1, 4, 0);
	}
	
	char chaine[20];
	sprintf_s(chaine,"largeur: %4.1lf",GL_coord->xmax-GL_coord->xmin);
	glFontTextOut(chaine, largeur/8-36, 10, 0);
	sprintf_s(chaine,"longueur: %4.1lf",GL_coord->ymax-GL_coord->ymin);
	glFontTextOut(chaine, largeur/8-36, 8, 0);
	sprintf_s(chaine,"xcentre: %4.1lf",(GL_coord->xmin+GL_coord->xmax)/2.0);
	glFontTextOut(chaine, largeur/8-50, 10, 0);
	sprintf_s(chaine,"ycentre: %4.1lf",(GL_coord->ymin+GL_coord->ymax)/2.0);
	glFontTextOut(chaine, largeur/8-50, 8, 0);
	sprintf_s(chaine,"sommets: %ld",dim*dim);
	glFontTextOut(chaine, largeur/8-25, 4, 0);

glFontEnd();

	glFlush();
	glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); // on restitue les param�tres pour la 3D

glEnable(GL_DEPTH_TEST);
}

void Plot::Repere(double Zsize)
{
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    gluLookAt(0,-8*GL_coord->Zfact,0,0,0,0,0,0,1);
    glRotated(GL_coord->angleX,1,0,0);
	glRotated(GL_coord->angleY,0,1,0);
    glRotated(GL_coord->angleZ,0,0,1);

	if(*mode>=1)//affichage des axes activ�
	{
	glBegin(GL_LINES);//dessine le rep�re

		glColor3ub(0,255,0);//z : vert
		glVertex3d(0,0,-16*Zsize);
		glVertex3d(0,0,16*Zsize);
		
		glColor3ub(0,0,255);//y : bleu
		glVertex3d(0,-160,0);
		glVertex3d(0,160,0);
		
		glColor3ub(255,0,0);//x : rouge
		glVertex3d(-160,0,0);
		glVertex3d(160,0,0);

	glEnd();

	glPushMatrix();

	GLUquadric* params;
	params = gluNewQuadric();

	glColor3ub(0,255,0);
	glTranslated(0,0,16*Zsize);
	glScaled(5,5,10);
	gluCylinder(params,1,0,2,20,1);
	glScaled(2,2,1);

	if(*mode==2)//axes gradu�s
	{
		glTranslated(0,0,-1.6*Zsize);
		for(int i=0;i<=11;i++)
		{
			glTranslated(0,0,0.1*Zsize);
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}
		glTranslated(0,0,-0.1*Zsize*12);
		for(int i=1;i<=12;i++)
		{
			glTranslated(0,0,-0.1*Zsize);
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}
	}
	glPopMatrix();

	glPushMatrix();

	glColor3ub(255,0,0);
	glRotated(90,0,1,0);
	glTranslated(0,0,160);
	glScaled(5,5,10);
	gluCylinder(params,1,0,2,20,1);
	glScaled(2,2,1);
		
	if(*mode==2)
	{
		glTranslated(0,0,-16);
		for(int i=0;i<=10;i++)
		{
			glTranslated(0,0,1.2*(200/12.0)/(GL_coord->xmax-GL_coord->xmin));
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}
		glTranslated(0,0,-1.2*(200/12.0)/(GL_coord->xmax-GL_coord->xmin)*11);
		for(int i=0;i<=10;i++)
		{
			glTranslated(0,0,-1.2*(200/12.0)/(GL_coord->xmax-GL_coord->xmin));
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}

	}
	glPopMatrix();

	glPushMatrix();

	glColor3ub(0,0,255);
	glRotated(-90,1,0,0);
	glTranslated(0,0,160);
	glScaled(5,5,10);
	gluCylinder(params,1,0,2,20,1);
	glScaled(2,2,1);

	if(*mode==2)
	{
		glTranslated(0,0,-16);
		for(int i=0;i<=10;i++)
		{
			glTranslated(0,0,1.2*(200/12.0)/(GL_coord->ymax-GL_coord->ymin));
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}
		glTranslated(0,0,-1.2*(200/12.0)/(GL_coord->ymax-GL_coord->ymin)*11);
		for(int i=0;i<=10;i++)
		{
			glTranslated(0,0,-1.2*(200/12.0)/(GL_coord->ymax-GL_coord->ymin));
			gluSphere(params,r_out*GL_coord->Zfact/130,10,10);
		}
	}
	glPopMatrix();

	gluDeleteQuadric(params);

	}
}

void Plot::camera(SDL_Event &event,double Zsize)
{
	bool continuer2=true;

	Coordon mouse={event.button.x,event.button.y};
	Coordon move={0,0};
	Graph_coord GL_0=*GL_coord;

	while(continuer2)
	{
		while(SDL_PollEvent(&event))
		{
			if(event.type==SDL_MOUSEBUTTONUP)
			{
				continuer2=false;
				break;
			}
			move.x=event.button.x-mouse.x;
			move.y=event.button.y-mouse.y;

			GL_coord->angleX =GL_0.angleX+ANGLE * move.y;
			GL_coord->angleZ =GL_0.angleZ+ANGLE * move.x;
						
			Dessiner(Zsize);						
		}
	}
}

void Plot::echelle(SDL_Event &event,double Zsize)
{
	Coordon mouse={event.button.x,event.button.y};
	Coordon move={0,0};
	Graph_coord GL_0=*GL_coord;

	bool continuer2=true;

	while(continuer2)
	{
		while(SDL_PollEvent(&event))
		{	
			if(event.type==SDL_MOUSEBUTTONUP)
			{
				continuer2=false;
				break;
			}

			move.y=mouse.y-event.button.y;

			if((move.y>0 && GL_0.Zfact+FACT * move.y<200) || (move.y<0 && GL_0.Zfact+FACT * move.y>5))
			{
				GL_coord->Zfact =GL_0.Zfact+FACT * move.y;
			}
						
			Dessiner(Zsize);
		}
	}
}

void Plot::boutons(SDL_Event &event,double Zsize)
{
	int i;

	if (event.button.button == SDL_BUTTON_LEFT) 
	{  
		for(i=0;i<4;i++)
		{
			if(bouton[i].appartient(event.button))
			{
				bouton[i].fonction(*GL_coord);
				Dessiner(Zsize);
				break;
			}
		}
		for(i=4;i<15;i++)
		{
			if (bouton[i].appartient(event.button))
			{
				bouton[i].fonction(*GL_coord);
				Init(0);
				Dessiner(Zsize);
				break;
			}
		}

		if (bouton[15].appartient(event.button))//bouton pas standard donc gestion "manuelle"
		{
			if(*reflet==1){*reflet=0;}
			else{*reflet=1;}

			Init(0);
			Dessiner(Zsize);
		}
		else if (bouton[16].appartient(event.button))
		{
			if(*mode==2){*mode=0;}
			else{*mode=*mode+1;}
		}
		else if (bouton[17].appartient(event.button))
		{
			if(*graphe==1){*graphe=0;}
			else{*graphe=1;}

			Init(0);
			Dessiner(Zsize);
		}
	} 
}

Noeud* Plot::Traduction(SDLKey Clav[],SDL_Event *event,double &Zsize)
{
SDL_KeyboardEvent *key=&event->key;

//si il y a une touche de modification enfonc�e ou si le Verr Num est d�sactiv� on ignore les touches
//sauf la combinaison "ctrl" + "v" (coller)
//et "Maj" + "/:"
//�vite les probl�mes de mauvaise association. ex : "�" au lieu de "^" 

if(!(key->keysym.mod & (KMOD_NUM)) || (key->keysym.mod & (KMOD_CAPS|KMOD_LSHIFT|KMOD_RSHIFT |KMOD_LCTRL|KMOD_RCTRL|KMOD_LALT|KMOD_RALT)))
{
	if((key->keysym.mod & (KMOD_LCTRL|KMOD_RCTRL)) && event->type==SDL_KEYDOWN && event->key.keysym.sym==SDLK_v)
	{
		char *str=GetClipboardText();
		if(str!=0)
		{
		Str.insert(position,str);
		position+=strlen(str);
		free(str);
		}
	}
	else if((key->keysym.mod & (KMOD_RSHIFT|KMOD_LSHIFT)) && event->type==SDL_KEYDOWN && event->key.keysym.sym==SDLK_PERIOD	)
	{
		Str.insert(position++,"/");
	}
	else
	{
		Erreur="ERREUR: MAJ/ALT enfoncee ou VERR_NUM desactive";
	}
}
else
{
	if(Clav[key->keysym.sym] == 0)
	{
		if(((key->keysym.unicode) != 0) && (key->keysym.unicode < 256) )
		{
			Clav[key->keysym.sym] = (SDLKey)key->keysym.unicode;
		}
		else
		{
			Clav[key->keysym.sym] = key->keysym.sym;
		}
	}

Erreur="";

/*	 13 : entree 
	 32 : espace
  40-43 : ( ) * + 
  45-47 : - . /  
  48-57 : 0-9 
   	 91 : ^
 97-122 : a-z 
  65-90 : A-Z   */

	if((Clav[key->keysym.sym]>=40 && Clav[key->keysym.sym]<= 57)||(Clav[key->keysym.sym]>=SDLK_a && Clav[key->keysym.sym]<= SDLK_z))
	{
		Str.insert(position++,SDL_GetKeyName(Clav[key->keysym.sym]));
	}
	else if(Clav[key->keysym.sym]==SDLK_LEFTBRACKET ||Clav[key->keysym.sym]==231 )//231:touche "9,�,^"
	{
		Str.insert(position++,"^");
		keybd_event(0x3,0,0,0);//on intercale une touche apr�s ^ pour eviter les �..
		keybd_event(0x3,0,KEYEVENTF_KEYUP,0);
	}
	else if(Clav[key->keysym.sym]==SDLK_SPACE)
	{
		Str.insert(position++," ");
	}
	else if(Clav[key->keysym.sym]==SDLK_LEFT && position>0)
	{
		position--;
	}
	else if(Clav[key->keysym.sym]==SDLK_RIGHT && position<Str.length())
	{
		position++;
	}
	else if(Clav[key->keysym.sym]==SDLK_RETURN)//entr�e
	{
		if(est_valide(Str))
		{
			if(Str.substr(0,4)=="val(")//�valuation en un point
			{
				Erreur=valeur(Str,arbre);//pas une erreur mais affich� de la m�me fa�on
				Str="";
				position=0;
			}
			else
			{
				//on retrace
				bool Reset=true;
				try
				{
					arbre=Construire_arbre(Str,arbre);
					Erreur="";
					Str2=Str;
				}
				catch(const std::string& chaine)
				{ 
					Erreur=chaine;
					arbre=Construire_arbre(Str2,arbre);//affiche la derni�re expression valide
					Reset=false;
				}

				glDeleteLists(*list, 1);
				*list = glGenLists(1);
				if(*list == 0)
				{
					fprintf(stderr, "erreur lors de la creation de la liste\n");//erreur
				}

				Init(0);

				Zsize=12.0;	
				if(Reset){Str="";position=0;}
			}
		}
		else
		{
			Erreur="ERREUR : caractere invalide";
		}
	}
	else if(Clav[key->keysym.sym]==SDLK_BACKSPACE && position>0)//touche <-
	{
		Str.erase(position-1,1);
		position--;
	}
}

return arbre;
}


void incr_maillage(Graph_coord &GL_coord)
{
	if(dim*dim<180000){dim*=FACTOR;}
}
void decr_maillage(Graph_coord &GL_coord)
{
	if(dim*dim>4000){dim/=FACTOR;}
}

void reset(Graph_coord &GL_coord)
{
	dim=230;
	GL_coord.angleZ=0.0;
	GL_coord.angleX=0.0;
	GL_coord.angleY=0.0;
	GL_coord.Zfact=20.0;
	GL_coord.xmin=XMIN;
	GL_coord.xmax=XMAX;
	GL_coord.ymin=YMIN;
	GL_coord.ymax=YMAX;
}