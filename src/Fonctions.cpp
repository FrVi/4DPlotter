#include "Fonctions.h"

bool est_definie(string Str)//dit si Str contient une fonction prise en charge ou bien une erreur
{
	if(Str=="conj"||Str=="abs"||Str=="arg"||Str=="exp"||Str=="log"||Str=="sqrt"||Str=="pow"||Str=="ln"||Str=="sin"||
	   Str=="cos"||Str=="tan"||Str=="cosh"||Str=="tanh"||Str=="sinh"||Str=="ch"||Str=="th"||Str=="sh"||Str=="gamma"||
	   Str=="re"||Str=="im"||Str=="arctan"||Str=="arccos"||Str=="arcsin"||Str=="argth"||Str=="argch"||Str=="argsh")
	{
		return true;
	}
	else
	{
		return false;
	}
}


int Priorite(string SubStr)
{
	if(SubStr=="+" || SubStr=="-") return 1;
	else if(SubStr=="/" || SubStr=="*") return 2;
	else if(SubStr=="^") return 3;
	else return 0;//pas un operateur/pile vide
}

double string_to_number(string Str)
{
	char c=Str[0];
	double nbr=0.0;
	double poids=0.1;
	unsigned int pos=0;

	while(c>='0' && c<='9')//on construit la partie enti�re du nombre � partir de ses chiffres
	{			
		nbr=nbr*10+c-'0';
		pos++;

		if(pos<Str.size())
		{
			c=Str[pos];
		}
		else
		{
			break;
		}
	}

	if(c=='.')//partie fractionnaire
	{
		pos++;

		if(pos<Str.size())
		{
			c=Str[pos];
		}
		//sinon, on ne change pas c, il reste egal � ".", la boucle while ne s'execute pas
			
		while(c>='0' && c<='9')//on construit la partie fractionnaire du nombre
		{		
			nbr=nbr+((double)(c-'0')*poids);
			poids=poids/10;
			pos++;

			if(pos<Str.size())
			{
				c=Str[pos];
			}
			else
			{
				break;
			}
		}
	}
	pos--;

	return nbr;
}


//1530=6*255, val-floor(val) : fonction //////, 
//puis on fabrique une fonction "triangulaire" /\/\/\,  
//puis on utilise min et max pour obtenir /--\__/--\__
int Couleur(double Arg)
{
	double val=Arg/(2*PI);
	return max(0,min(255,(int)floor(0.5+(1530.*(fabs(val-floor(val)-0.5)-1/6.)))));
}

Uint32 definir_couleur(double Arg)
{
	return (Couleur(Arg)<<16)|(Couleur(Arg-2*PI/3)<<8)|(Couleur(Arg+2*PI/3));
}


// (fusion algorithme de postfixage (Shunting Yard Algorithm) et de la construction d'arbre)
Noeud*  Construire_arbre(string Str,Noeud* arbre)//renvoie l'expression d'entree sous forme d'arbre
{
	bool cond=true;

	int prio=0;
	stack<string,vector<string> > pile;
	string SubStr;
	char c;
	unsigned int pos;

	complex<double> nbr;

	stack<Noeud*,vector<Noeud*> > pile2;

	NoeudNum *nombre=NULL;
	NoeudVar *variable=NULL;
	NoeudOp *operateur=NULL;

	for(pos=0;pos<Str.size();pos++)
	{
		c=Str[pos];

		if((c>='0' && c<='9') || c=='.')//on cherche le nombre dont "c" est un chiffre.
		{
			nbr=Construire_nombre(Str,pos,c);
			nombre=new NoeudNum(nbr);
			pile2.push(nombre);//on empile l'adresse du noeud
		}
		else if(c=='(')
		{
			SubStr=c;
			pile.push(SubStr);
		} 
		else if(c==')')
		{
			while(pile.size()!=0 && pile.top()!="(")//si pas de parenth�se gauche trouv�e et pile vide: erreur
			{
				gestion_operateurs(pile2,pile.top());
				pile.pop();
			}

			if(pile.size()==0)
			{
				liberer(pile2);
				throw string("ERREUR : oubli de parenthese ouvrante");
			}
			pile.pop();

			if(pile.size()!=0 && est_definie(pile.top()))//si le sommet de la pile est une fonction connue
			{
				verifier(pile2);
				operateur=new NoeudOp(pile2.top(),pile.top(),NULL);
				pile2.pop();

				if(pile.top()=="pow"||pile.top()=="gamma")//2 seules fonctions de deux variables pour le moment
				{
					verifier(pile2);
					operateur->set_fils_droit(pile2.top());
					operateur->inverser_fils();
					pile2.pop();
				}

				pile2.push(operateur);
				pile.pop();
			}
		}
		else if(c=='+' || c=='-' || c=='*' || c=='/')//left associated operators
		{
			SubStr=c;
			if(est_moins_unaire(c,Str,pos))//moins unaire est variable rejet� pour le moment
			{
				pos++;
				c=Str[pos];
				nbr=Construire_nombre(Str,pos,c);
				nombre=new NoeudNum(-nbr);
				pile2.push(nombre);
			}
			else
			{
				prio=Priorite(SubStr);
				while(pile.size()!=0 && pile.top()!="(" && Priorite(pile.top())>=prio)//priorite de c <= sommet de la pile (vide= priorit� 0)
				{
					gestion_operateurs(pile2,pile.top());
					pile.pop();
				}
				pile.push(SubStr);
			}
		}
		else if(c=='^')
		{
			SubStr=c;
			pile.push(SubStr);
		}
		else if(c=='x' || c=='y' || c=='z')
		{
			variable=new NoeudVar(c);
			pile2.push(variable);
		}
		else if(c=='i'&& ((pos<Str.size()-1  &&Str[pos+1]!='m')||pos==Str.size()-1) )//diff�rentie le nombre i et le i de Im()
		{
			nombre=new NoeudNum(0.0,1.0);
			pile2.push(nombre);
		}
		else if(c=='p' && (pos<Str.size()-1  &&Str[pos+1]=='i') )//nombre pi
		{
			pos++;
			nombre=new NoeudNum(PI,0.0);
			pile2.push(nombre);
		}
		else if(c=='e'&& ((pos<Str.size()-1  &&Str[pos+1]!='x')||pos==Str.size()-1) )//diff�rentie le nombre e et le e de exp()
		{
			nombre=new NoeudNum(E,0.0);
			pile2.push(nombre);
		}
		else if(c=='a' && (pos<Str.size()-2 && Str[pos+1]=='n' && Str[pos+2]=='s'))//ans d�signe le r�sultat pr�c�dent
		{
			pos=pos+2;
			pile2.push(arbre->copier());
			cond=false;
		}
		else if(c==',')//separateur
		{
			if(pile.size()==0)
			{
				liberer(pile2);
				throw string("ERREUR : virgule mal placee");
			}//le separateur n'a de sens que dans une parenth�se : pow(x,2)

			while(pile.top()!="(")
			{
				gestion_operateurs(pile2,pile.top());
				pile.pop();

				if(pile.size()==0)
				{
					liberer(pile2);
					throw string("ERREUR : virgule mal placee");
				}
			}
		}
		else if(c!=' ')//fonctions (peuvent �tre invalides)
		{
			SubStr="";

			while((c>='a' && c<='z')|| (c>='A' && c<='Z'))
			{
				SubStr=SubStr+c;
				pos++;

				if(pos<Str.size())
				{
					c=Str[pos];
				}
				else
				{
					break;
				}
			}
			pos--;
			pile.push(SubStr);
		}
	}

	while(pile.size()!=0)
	{
		if(pile.top()=="(")
		{
			liberer(pile2);
			throw string("ERREUR : oubli de parenthese fermante");
		}
		else
		{
			gestion_operateurs(pile2,pile.top());
			pile.pop();
		}
	}

	if(cond && arbre!=NULL)
	{
		delete arbre;
	}

	return pile2.top();
}

bool est_chiffre(string Str, int pos)
{
	return ((Str[pos]>='0' && Str[pos]<='9') || Str[pos]=='.');
}

bool est_variable(string Str, int pos)
{
	return (Str[pos]=='x' || Str[pos]=='y' || Str[pos]=='z');
}

bool est_moins_unaire(char c,string Str, int pos)
{
	return (c=='-' && pos<Str.size() && est_chiffre(Str,pos+1) && (pos==0 || (pos>0 && !est_variable(Str,pos-1) && !est_chiffre(Str,pos-1))));
}

void liberer( stack<Noeud*,vector<Noeud*> > &pile)
{
	while(pile.size()!=0)
	{
		delete pile.top();
		pile.pop();
	}
}

void gestion_operateurs(stack<Noeud*,vector<Noeud*> > &pile2,string top)
{
	char c=top[0];
	NoeudOp *operateur=NULL;

	if(c=='+' || c=='-' || c=='*' || c=='/' || c=='^')
	{
		verifier(pile2);
		operateur=new NoeudOp(NULL,top,pile2.top());
		pile2.pop();

		verifier(pile2);
		operateur->set_fils_gauche(pile2.top());
		pile2.pop();

		pile2.push(operateur);
	}
	else
	{
		throw string("ERREUR : expression invalide");
	}	
}

void verifier(stack<Noeud*,vector<Noeud*> > &pile)
{
	if(pile.size()==0)
	{
		liberer(pile);
		throw string("ERREUR : argument manquant");//pile vide lors d'une tentative de lecture
	}
}

double Construire_nombre(string Str,unsigned int &pos,char &c)
{
	string SubStr="";
	while((c>='0' && c<='9')|| c=='.')
	{
		SubStr=SubStr+c;
		pos++;

		if(pos<Str.size())
		{
			c=Str[pos];
		}
		else
		{
			break;
		}
	}

	pos--;

	return string_to_number(SubStr);
}

string valeur(string Str,Noeud* arbre)
{
	complex<double> i;
	unsigned int k=4;//"val(...)" 4-i�me caract�re de l'expression: contenu de la parenth�se
	char c;

    if(Str.length()>=5)
	{
		if((Str[4]>='0' && Str[4]<='9') || Str[4]=='.' ||(Str[4]=='-' && Str.length()>=6))
		{
			if((Str[4]>='0' && Str[4]<='9') || Str[4]=='.')
			{
				c=Str[4];
				i=Construire_nombre(Str,k,c);
			}
			else
			{
				c=Str[5];
				k=5;
				i=-Construire_nombre(Str,k,c);
			}

			if(Str.length()>=k+3 && Str[k+1]==',' )
			{
				if(((Str[k+2]>='0' && Str[k+2]<='9') || Str[k+2]=='.' ||(Str[k+2]=='-' && Str.length()>=k+4)))
				{
					if((Str[k+2]>='0' && Str[k+2]<='9') || Str[k+2]=='.')
					{
						k=k+2;
						c=Str[k];
						i.imag(Construire_nombre(Str,k,c));
					}
					else
					{
						c=Str[k+3];
						k=k+3;
						i.imag(-Construire_nombre(Str,k,c));
					}

					i=arbre->calculer(i);
					char str[100];
					sprintf_s(str,"%4.5lf+%4.5lfi",i.real(),i.imag());
					return str;
				}
				else{return "ERREUR, second parametre de val(x,y) invalide";}
			}
			else{return "ERREUR, second argument de val(x,y) manquants";}
		}
		else{return "ERREUR, premier parametre de val(x,y) invalide";}
	}
	else{return "ERREUR, arguments de val(x,y) manquants";}      
}    

void set_tab(Coordonnees *tab,complex<double> t,int j,int mode)
{
	if(mode==0)
	{
		tab[j].z=min(abs(t),100000.);//p�les clamp�s � 100000.
		tab[j].color=definir_couleur(arg(t));    
	}
	else
	{
		tab[j].z=-min(abs(t),100000.);
		tab[j].color=definir_couleur(arg(t)+PI);  
	}
}

char *GetClipboardText(void)
{
	if(OpenClipboard(0))
	{
		HANDLE h =GetClipboardData(CF_TEXT);
		if(h)
		{
			char *s = (char *)GlobalLock(h);
			GlobalUnlock(h);
			CloseClipboard();
			return _strdup(s);
		}
		CloseClipboard();
	}

	return 0;
}

bool est_valide(string Str)
{
	char c;

	if(Str==string(Str.length(),' ') )
	{
		return false;
	}
	else
	{
		for(int i=0;i<Str.length();i++)
		{
			c=Str[i];
			if( !(c==' ' || (c>='a' && c<='z') || (c>='(' && c<='+') || (c>='-' && c<='9') || c=='^'|| c==','))
			{
				return false;
			}
		}
	}
	return true;
}

complex<double> Gamma(complex<double> z,complex<double> n)
{
	int imax=max(0,(int)n.real());//si n invalide, on le corrige 
	complex<double> val(1.0,0.0);

	for(double i=1.0;i<=imax;i++)
	{
		val*=i/(z+i);
	}

	val*=pow((double)imax,z)/z;

	return val;
}