#ifndef BTN
#define BTN

#include "Fonctions.h"

//La classe de bouton utilis�e par la fen�tre via la classe Plot

class Bouton
{
	public :
		Bouton();
		Bouton(int Xmin,int Xmax,int Ymin, int Ymax,string Str,void (*Fonction)(Graph_coord &),GLubyte red,GLubyte green,GLubyte blue);
		void set(int Xmin,int Xmax,int Ymin, int Ymax,string Str,void (*Fonction)(Graph_coord &),GLubyte red,GLubyte green,GLubyte blue);
		void afficher();//affiche le bouton dans le menu 
		void (*fonction)(Graph_coord &);//fonction associ�e au bouton
		bool appartient(SDL_MouseButtonEvent button);//compare coordonn�es clic bouton
		~Bouton();

	private:
		int xmin;
		int xmax;
		int ymin;
		int ymax;
		string str;
		GLubyte red;
		GLubyte green;
		GLubyte blue;
};


/*les fonctions qui seront associ�es aux boutons sauf :
	void incr_maillage(Graph_coord &GL_coord);
	void decr_maillage(Graph_coord &GL_coord);
	void reset(Graph_coord &GL_coord);
qui agissent sur dim, variable globale au fichier Plot.cpp
*/

//fonctions de d�placement rapide de la cam�ra
	void Vue_x(Graph_coord &GL_coord);
	void Vue_y(Graph_coord &GL_coord);
	void Vue_z(Graph_coord &GL_coord);
	void Vue_diag(Graph_coord &GL_coord);

//modifie la taille de la plage d'abscisse et d'ordonn�e repr�sent�e
	void incr_largeur(Graph_coord &GL_coord);
	void decr_largeur(Graph_coord &GL_coord);
	void incr_longueur(Graph_coord &GL_coord);
	void decr_longueur(Graph_coord &GL_coord);

//d�place le centre de la plage d'abscisse et d'ordonn�e repr�sent�e
	void incr_x_centre(Graph_coord &GL_coord);
	void decr_x_centre(Graph_coord &GL_coord);
	void incr_y_centre(Graph_coord &GL_coord);
	void decr_y_centre(Graph_coord &GL_coord);

#endif