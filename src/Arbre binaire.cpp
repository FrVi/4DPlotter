#include "Arbre Binaire.h"

Noeud::Noeud(){}

Noeud::~Noeud(){}

NoeudNum::NoeudNum()
{
	val=0;
}

NoeudNum::NoeudNum(complex<double> v)
{
	val=v;
}

NoeudNum::NoeudNum(double x, double y)
{
	complex<double> v(x,y);
	val=v;
}

NoeudNum::~NoeudNum(){}

complex<double> NoeudNum::calculer(complex<double> z)
{
	return val;
}

void NoeudNum::set_valeur(complex<double> v)
{
	val=v;
}	

void NoeudNum::set_valeur(double x, double y)
{
	complex<double> v(x,y);
	val=v;
}

NoeudNum* NoeudNum::copier()
{
	return new NoeudNum(val);
}

NoeudOp::NoeudOp()
{
	g=NULL;
	d=NULL;
}

NoeudOp::NoeudOp(Noeud* gauche, string v,Noeud* droit )
{
	g=gauche;
	d=droit;
	val=v;
}

NoeudOp::~NoeudOp()
{
	if(g!=NULL)
	{
		delete g;
	}

	if(d!=NULL)
	{
		delete d;
	}
}

void NoeudOp::inverser_fils()
{
	Noeud* temp;
	temp=g;
	g=d;
	d=temp;
}

complex<double> NoeudOp::calculer(complex<double> z)//ne verifie pas que les fils sont definis: pas de gestion d'erreur
{
	if(val=="+")
	{
		return g->calculer(z) + d->calculer(z) ;
	}
	else if(val=="-")
	{
		if(d!=NULL)//- binaire
		{
			return g->calculer(z) - d->calculer(z) ;
		}
		else//- unaire
		{
			return (-1.0)*(g->calculer(z)) ;
		}
	}
	else if(val=="*")
	{ 
		if(g!=NULL && d!=NULL)
		{
			return g->calculer(z) * d->calculer(z) ;
		}
		else
		{
			return 0.0;
		}
	}
	else if(val=="/")
	{
		complex<double> droit=d->calculer(z);
		if(real(droit)==0 && imag(droit)==0 )
		{
			droit.real(0.0000001);
			return g->calculer(z) / droit;
		}
		else
		{
			return g->calculer(z) / droit ;
		}
	}
	else if(val=="^" || val=="pow")
	{
		return pow(g->calculer(z) , d->calculer(z) );
	}
	else if(val=="abs")
	{
		return abs(g->calculer(z));
	}
	else if(val=="conj")
	{
		return conj(g->calculer(z));
	}
	else if(val=="arg")
	{
		return arg(g->calculer(z));
	}
	else if(val=="exp")
	{
		return exp(g->calculer(z));
	}
	else if(val=="sin")
	{
		return sin(g->calculer(z));
	}
	else if(val=="cos")
	{
		return cos(g->calculer(z));
	}
	else if(val=="tan")
	{
		return tan(g->calculer(z));
	}
	else if(val=="cosh" || val=="ch")
	{
		return cosh(g->calculer(z));
	}
	else if(val=="sinh" || val=="sh")
	{
		return sinh(g->calculer(z));
	}
	else if(val=="tanh" || val=="th")
	{
		return tanh(g->calculer(z));
	}
	else if(val=="log" || val=="ln")
	{
		return log(g->calculer(z));
	}
	else if(val=="sqrt")
	{
		return sqrt(g->calculer(z));
	}
	else if(val=="gamma")
	{
		return Gamma(g->calculer(z),d->calculer(z));
	}
	else if(val=="re")
	{
		return g->calculer(z).real();
	}
	else if(val=="im")
	{
		return g->calculer(z).imag();
	}
	else if(val=="arctan")
	{
		complex<double> I(0,1);
		complex<double> valeur=I*g->calculer(z);
		complex<double> U(1,0);

		return I/2.0*(log(U-valeur)-log(U+valeur));
	}
	else if(val=="arcsin")
	{
		complex<double> I(0,1);
		complex<double> valeur=g->calculer(z);
		complex<double> U(1,0);

		return -I*log(I*valeur+sqrt(U-valeur*valeur));
	}
	else if(val=="arccos")
	{
		complex<double> I(0,1);
		complex<double> valeur=g->calculer(z);
		complex<double> U(1,0);

		return -I*log(valeur+I*sqrt(U-valeur*valeur));
	}
	else if(val=="argch")
	{
		complex<double> valeur=g->calculer(z);
		complex<double> U(1,0);

		return log(valeur+sqrt(valeur-U)*sqrt(valeur+U));
	}
	else if(val=="argsh")
	{
		complex<double> valeur=g->calculer(z);
		complex<double> U(1,0);

		return log(valeur+sqrt(valeur*valeur+U));
	}
	else if(val=="argth")
	{
		complex<double> valeur=g->calculer(z);
		complex<double> U(1,0);

		return U/2.0*(log(U+valeur)-log(U-valeur));
	}
	//pas d'erreur possible ici: surviennent lors de la construction de l'arbre

	return z;
}

void NoeudOp::set_valeur(string v)
{
	val=v;
}

void NoeudOp::set_fils(Noeud* gauche,Noeud* droit)
{
	g=gauche;
	d=droit;
}

void NoeudOp::set_fils_droit(Noeud* droit)
{
	d=droit;
}

void NoeudOp::set_fils_gauche(Noeud* gauche)
{
	g=gauche;
}

NoeudOp* NoeudOp::copier()
{
	if(g!=NULL)
	{	
		if(d!=NULL)
		{
				return new NoeudOp(g->copier(),val,d->copier());
		}
		else
		{
			return new NoeudOp(g->copier(),val,NULL);
		}
	}
	else
	{
		if(d!=NULL)
		{
				return new NoeudOp(NULL,val,d->copier());
		}
		else
		{
			return new NoeudOp(NULL,val,NULL);
		}
	}
}

NoeudVar::NoeudVar()
{
	val=0;
}

NoeudVar::NoeudVar(char v)
{
	val=v;
}

NoeudVar::~NoeudVar(){}

complex<double> NoeudVar::calculer(complex<double> z)
{
	if(val=='x')
	{
		return z.real();
	}
	else if(val=='y')
	{
		return z.imag();
	}
	else if(val=='z')
	{
		return z;
	}
	else if(val=='i')
	{
		complex <double> i(0.0, 1.0);
		return i;
	}
 //pas d'autres possiblitiés: autres caractères reconnus comme fonction,
}

void NoeudVar::set_valeur(char v)
{
	val=v;
}

NoeudVar* NoeudVar::copier()
{
	return new NoeudVar(val);
}