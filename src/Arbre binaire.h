#ifndef HEADER3
#define HEADER3

#include <stdlib.h> 
#include <stdio.h> 
#include <SDL/SDL.h> 

#include <iostream>
#include <string>
#include <math.h>
#include <complex>

using namespace std;

complex<double> Gamma(complex<double> z,complex<double> n);//fonction Gamma d'Euler

	//classe abstraite
	class Noeud
	{
		public :
			Noeud();
			virtual ~Noeud();
			virtual complex<double> calculer(complex<double> z) = 0;
			virtual Noeud* copier() = 0;
     };

//noeud pouvant contenir un nombre complexe
	class NoeudNum : public Noeud
	{
		public :
			NoeudNum();
			NoeudNum(complex<double> v);
			NoeudNum(double x, double y);
			virtual ~NoeudNum();
			virtual complex<double> calculer(complex<double> z);
			void set_valeur(complex<double> v);
			void set_valeur(double x, double y);
			virtual NoeudNum* copier();

		private: complex<double> val; 
	};

//contient un op�rateur (cha�ne) et des pointeurs vers les op�randes
	class NoeudOp : public Noeud
	{
		public :
			NoeudOp();
			NoeudOp(Noeud* gauche, string v,Noeud* droit );
			virtual ~NoeudOp();
			virtual complex<double> calculer(complex<double> z);
			void set_valeur(string v);
			void set_fils(Noeud* gauche,Noeud* droit);
			void set_fils_droit(Noeud* droit);
			void set_fils_gauche(Noeud* gauche);
			void inverser_fils();
			virtual NoeudOp* copier();

		private:
			string val; 
			Noeud* g;
			Noeud* d;
	};

//contient une variable (caract�re) : x, y ou z
	class NoeudVar : public Noeud
	{
		public :
			NoeudVar();
			NoeudVar(char v);
			virtual ~NoeudVar();
			void set_valeur(char v);
			virtual complex<double> calculer(complex<double> z);
			virtual NoeudVar* copier();

		private:  char val; 
	};

#endif