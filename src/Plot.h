#ifndef FONCTIONS
#define FONCTIONS

#include "Bouton.h"

class Plot
{
  public :

Plot(int *mode,int *reflet,Bouton *Bouton,GLuint *list,GLFONT *Font,Graph_coord *GL_coord,Noeud* arbre,int *graphe,string STR, string STR2);
~Plot();
void Init(int m);//initialise la display list � l'aide de l'arbre d'expression
void Dessiner(double Zsize);//dessine le graphe (appelle Menu et Repere)
void Menu();//affiche les boutons et le reste du menu
void Repere(double Zsize);//affiche le rep�re dans la fen�tre
void camera(SDL_Event &event,double Zsize);//gestion du mouvement de la cam�ra
void echelle(SDL_Event &event,double Zsize);//gestion de l'echelle verticale
void boutons(SDL_Event &event,double Zsize);//gestion des boutons (fonctions dans Bouton.h)
Noeud* Traduction(SDLKey Clav[],SDL_Event *event,double &Zsize);//gestion touches du clavier 

  private:

int *graphe;
int *mode;
int *reflet;
int position;

Bouton *bouton;
GLuint *list;
GLFONT *Font;
Graph_coord *GL_coord;
Noeud* arbre;
string Str;
string Str2;
string Erreur;	

};

//fonctions agissant sur la variable globale dim de Plot.cpp
void incr_maillage(Graph_coord &GL_coord);//augmente le nombre de points du maillage 
void decr_maillage(Graph_coord &GL_coord);//reduit le nombre de points
void reset(Graph_coord &GL_coord);//remet les param�tres du graphe � leurs valeurs initiales

#endif