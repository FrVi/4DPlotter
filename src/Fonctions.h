#ifndef FCN
#define FCN

#include "Arbre Binaire.h"

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <queue>
#include <stack>
#include <vector>
#include "glfont.h"

//D�finition des constantes utilis�es par le programme

#define PI 3.1415926535897931
#define E  2.7182818284590452

#define largeur 1024//dim de la fen�tre (bug si >1936)
#define hauteur 1024//(superposition si <1024)

#define h_menu 128//dim de la bande menu
#define menu_r  212
#define menu_g  212
#define menu_b  212

#define txt_r 0//couleur du texte du menu
#define txt_g 0
#define txt_b 0

#define ANGLE 0.5
#define FACT 0.2

#define FACTOR 1.2
#define INCR 1.0

#define NBR_BOUTON 18

#define POS_x 60//position des boutons "+" et "-"
#define POS_y 1//position des boutons "+" et "-"
#define r_out 0.3

#define XMIN (-100.0/12.0)
#define XMAX (100.0/12.0)
#define YMIN (-100.0/12.0)
#define YMAX (100.0/12.0)


//D�finition des structures de donn�es utilis�es par le programme, sauf la classe d'arbre (Noeud)

typedef struct Coordonnees Coordonnees;
struct Coordonnees
{
	double z;
	Uint32 color;
};

typedef struct Coord Coord;
struct Coord
{
	int xmin;
	int xmax;
	int ymin;
	int ymax;
};

typedef struct Coordon Coordon;
struct Coordon
{
	int x;
	int y;
};

typedef struct Graph_coord Graph_coord;
struct Graph_coord
{
	double angleZ;
	double angleX;
	double angleY;
	double Zfact;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
};

//fonctions utiles au programme

int Couleur(double Arg);//associe argument et composante couleur Rouge, Bleue ou Verte
Uint32 definir_couleur(double Arg);//combine les 3 composantes couleurs

bool est_definie(string Str);//dit si Str contient une fonction prise en charge ou bien une erreur
int Priorite(string SubStr);//priorit� de l'op�rateur dans SubStr

double string_to_number(string Str);//convertit une cha�ne en nombre
double Construire_nombre(string Str,unsigned int &pos,char &c);//identifie et renvoie (via string_to_number) le nombre contenu dans Str (c: premier chiffre)

Noeud* Construire_arbre(string Str,Noeud* arbre);//renvoie l'expression d'entree sous forme d'arbre (fusion algorithme de postfixage et de construction d'arbre)
void gestion_operateurs(stack<Noeud*,vector<Noeud*> > &pile2,string top);
void liberer( stack<Noeud*,vector<Noeud*> > &pile);//lib�re les allocations dynamiques en cas d'erreur dans la construction de l'arbre
void verifier(stack<Noeud*,vector<Noeud*> > &pile);//verifie que la pile est bien non vide quand on tente d'acc�der � un de ses �l�ments
void set_tab(Coordonnees *tab,	complex<double> t,int j,int mode);//place arg (un repr�sentant) et module de t dans tab, ou -module et arg+Pi (selon le mode)

bool est_chiffre(string Str, int pos);
bool est_variable(string Str, int pos);//fonction d�terminant si Str[pos] est "x", "y" ou  "z"
bool est_moins_unaire(char c,string Str, int pos);//identifie le moins unaire

string valeur(string Str,Noeud* arbre);//fonction d'�valuation de l'arbre en un point d'affixe stock�e dans Str

char *GetClipboardText(void);//fonction utilis�e pour le copier-coller
bool est_valide(string Str);//fonction v�rifiant la validit� de l'expression (on peut copier-coller des caract�res invalides)

complex<double> Gamma(complex<double> z,complex<double> n);//fonction Gamma d'Euler

#endif